#!/usr/bin/env python
#-----------------------------------------------------------------------------
# Copyright (c) 2012 - 2018, Anaconda, Inc. and Intake contributors
# All rights reserved.
#
# The full license is in the LICENSE file, distributed with this software.
#-----------------------------------------------------------------------------

from setuptools import setup, find_packages
import versioneer

INSTALL_REQUIRES = ['intake >=0.5.2']

setup(
    name='intake-cerbere',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Cerbere plugins for Intake',
    url='',
    maintainer='Jeff Piolle',
    maintainer_email='jfpiolle@ifremer.fr',
    license='BSD',
    py_modules=['intake_cerbere'],
    packages=find_packages(),
    entry_points={
        'intake.drivers': [
            'cdataset = intake_cerbere.cdataset:CDatasetSource',
            'cfeature = intake_cerbere.cfeature:CFeatureSource',
        ]
    },
    package_data={'': ['*.csv', '*.yml', '*.html']},
    include_package_data=True,
    install_requires=INSTALL_REQUIRES,
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    zip_safe=False,
)

import fsspec

from . import __version__
from .cdataset import CDatasetSource


class CFeatureSource(CDatasetSource):
    """Cerbere Dataset """
    version = __version__
    container = 'cerbere-feature'
    partition_access = True

    name = 'cfeature'

    def __init__(self, urlpath, feature, **kwargs):
        self.feature = feature
        super(CFeatureSource, self).__init__(urlpath, **kwargs)

    def _open_dataset(self):
        import cerbere
        url = self.urlpath

        kwargs = self.cerbere_kwargs

        _open_as_feature = cerbere.open_as_feature
        url = fsspec.open_local(url, **self.storage_options)

        self._ds = _open_as_feature(
            self.feature, url, plugin=self.plugin, chunks=self.chunks, **kwargs
        )

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions

import intake  # Import this first to avoid circular imports during discovery.
from intake.container import register_container
from .cdataset import CDatasetSource
from .cfeature import CFeatureSource


intake.register_driver('cerbere-dataset', CDatasetSource)
intake.register_driver('cerbere-feature', CFeatureSource)

register_container('cerbere-dataset', CDatasetSource)


